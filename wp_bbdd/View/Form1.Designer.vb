﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox_Producto = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_PrecioUnitario = New System.Windows.Forms.TextBox()
        Me.NumericUpDown_Cantidad = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_Total = New System.Windows.Forms.TextBox()
        Me.Button_Add = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        CType(Me.NumericUpDown_Cantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Producto"
        '
        'ComboBox_Producto
        '
        Me.ComboBox_Producto.FormattingEnabled = True
        Me.ComboBox_Producto.Location = New System.Drawing.Point(13, 30)
        Me.ComboBox_Producto.Name = "ComboBox_Producto"
        Me.ComboBox_Producto.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox_Producto.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(144, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "PrecioUnitario"
        '
        'TextBox_PrecioUnitario
        '
        Me.TextBox_PrecioUnitario.Location = New System.Drawing.Point(147, 30)
        Me.TextBox_PrecioUnitario.Name = "TextBox_PrecioUnitario"
        Me.TextBox_PrecioUnitario.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_PrecioUnitario.TabIndex = 3
        '
        'NumericUpDown_Cantidad
        '
        Me.NumericUpDown_Cantidad.Location = New System.Drawing.Point(267, 30)
        Me.NumericUpDown_Cantidad.Name = "NumericUpDown_Cantidad"
        Me.NumericUpDown_Cantidad.Size = New System.Drawing.Size(51, 20)
        Me.NumericUpDown_Cantidad.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(267, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Cantidad"
        '
        'TextBox_Total
        '
        Me.TextBox_Total.Location = New System.Drawing.Point(337, 30)
        Me.TextBox_Total.Name = "TextBox_Total"
        Me.TextBox_Total.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_Total.TabIndex = 6
        '
        'Button_Add
        '
        Me.Button_Add.Location = New System.Drawing.Point(458, 30)
        Me.Button_Add.Name = "Button_Add"
        Me.Button_Add.Size = New System.Drawing.Size(75, 23)
        Me.Button_Add.TabIndex = 7
        Me.Button_Add.Text = "Agregar"
        Me.Button_Add.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(337, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Total"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(13, 58)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(520, 231)
        Me.DataGridView1.TabIndex = 9
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(550, 301)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button_Add)
        Me.Controls.Add(Me.TextBox_Total)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.NumericUpDown_Cantidad)
        Me.Controls.Add(Me.TextBox_PrecioUnitario)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBox_Producto)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.NumericUpDown_Cantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents ComboBox_Producto As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox_PrecioUnitario As TextBox
    Friend WithEvents NumericUpDown_Cantidad As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox_Total As TextBox
    Friend WithEvents Button_Add As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents DataGridView1 As DataGridView
End Class
